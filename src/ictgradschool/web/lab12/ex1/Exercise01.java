package ictgradschool.web.lab12.ex1;

import ictgradschool.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
//        Map<String, String> articleMap = new HashMap<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");


            while (true) {
                System.out.printf("input a partial title of an article: ");
                String s = "%" + Keyboard.readInput() + "%";
                if (s.equals("%%")) {continue;}
                try (PreparedStatement stmt = conn.prepareStatement("SELECT title, body FROM simpledao_articles WHERE title LIKE ? ;")) {

                    stmt.setString(1, s);
                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            System.out.println(r.getString("body"));
                        }
                    }

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
