package ictgradschool.web.lab12.ex2;

/**
 * Created by mshe666 on 4/01/2018.
 */
public class Article {
    private String title;
    private String body;

    Article(String title, String body) {
        this.title = title;
        this.body = body;
    }
    void setTitle(String s) {
        this.title = s;
    }
    void setBody(String s) {
        this.body = s;
    }
    String getTitle() {
        return title;
    }
    String getBody() {
        return body;
    }

}
