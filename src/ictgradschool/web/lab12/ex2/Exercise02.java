package ictgradschool.web.lab12.ex2;

import ictgradschool.Keyboard;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Exercise02 {

    public static void main(String[] args) throws IOException, SQLException {
        try (ArticleDAO dao = new ArticleDAO(new MySQLDatabase())) {

            while (true) {
                System.out.printf("input a partial title of an article: ");
                String s = "%" + Keyboard.readInput() + "%";
                if (s.equals("%%")) {continue;}
//                System.out.println("All lecturers: ");
                List<Article> articles = dao.allArticles(s);
                for (Article article : articles) {

                    System.out.println(article.getBody());

                }
            }


        }

    }
}
