package ictgradschool.web.lab12.ex2;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mshe666 on 4/01/2018.
 */
public class ArticleDAO implements AutoCloseable {
    private final Database db;
    private final Connection conn;

    public ArticleDAO(Database db) throws IOException, SQLException {
        this.db = db;
        this.conn = db.getConnection();
    }

    public List<Article> allArticles(String s) throws SQLException{

        try (PreparedStatement stmt = conn.prepareStatement("SELECT title, body FROM simpledao_articles WHERE title LIKE ? ;")) {
            stmt.setString(1, s);
            try (ResultSet rs = stmt.executeQuery()) {

                List<Article> articles = new ArrayList<>();
                while (rs.next()) {
                    articles.add(articleFromResultSet(rs));
                }

                return articles;

            }
        }
    }

    private Article articleFromResultSet(ResultSet rs) throws SQLException {
        return new Article(rs.getString(1), rs.getString(2));
    }

    @Override
    public void close() throws SQLException {
        this.conn.close();
    }


}
